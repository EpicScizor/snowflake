#[derive(Debug, Copy, Clone)]
pub enum NeuronType {
    InputOutput,
    Relu,
    Sigmoid,
    BinaryStep,
    Tanh,
    Softplus,
    Gaussian,
    LeakyRelu,
}
impl Default for NeuronType {
    fn default() -> Self {
        NeuronType::InputOutput
    }
}
//
// INPUT NEURONS
//
#[derive(Default, Debug, Clone)]
pub struct Neuron {
    pub value: f32,
    pub links: Vec<(usize, f32)>,
    pub neuron_type: NeuronType,
}
impl Neuron {
    fn evaluate(&self) -> f32 {
        match self.neuron_type {
            NeuronType::InputOutput => self.value,
            NeuronType::Relu => self.value.max(0.),
            NeuronType::Sigmoid => 1. / (1. + (-self.value).exp()),
            NeuronType::BinaryStep => {
                if self.value < 0. {
                    0.
                } else {
                    1.0
                }
            }
            NeuronType::Tanh => {
                let ex = self.value.exp();
                let emx = (-self.value).exp();
                (ex - emx) / (ex + emx)
            }
            NeuronType::Softplus => (1. + self.value.exp()).ln(),
            NeuronType::Gaussian => (-self.value).powi(2).exp(),
            NeuronType::LeakyRelu => {
                if self.value < 0. {
                    0.01 * self.value
                } else {
                    self.value
                }
            }
        }
    }
    fn add(&mut self, x: f32) {
        self.value += x;
    }
    fn clear(&mut self) {
        self.value = 0.;
    }
    fn get_links(&mut self) -> &mut Vec<(usize, f32)> {
        &mut self.links
    }

    fn typed(neuron_type: NeuronType) -> Neuron {
        let mut n = Neuron::default();
        n.neuron_type = neuron_type;
        n
    }
    // Neuron constructors
    pub fn relu() -> Neuron {
        Self::typed(NeuronType::Relu)
    }
    pub fn sigmoid() -> Neuron {
        Self::typed(NeuronType::Sigmoid)
    }
    pub fn binary_step() -> Neuron {
        Self::typed(NeuronType::BinaryStep)
    }
    pub fn tanh() -> Neuron {
        Self::typed(NeuronType::Tanh)
    }
    pub fn softplus() -> Neuron {
        Self::typed(NeuronType::Softplus)
    }
    pub fn gaussian() -> Neuron {
        Self::typed(NeuronType::Gaussian)
    }
    pub fn leaky_relu() -> Neuron {
        Self::typed(NeuronType::LeakyRelu)
    }
}
//
// NET
//
#[derive(Debug, Copy, Clone)]
pub struct NetConfig {
    pub min_weight_magnitude: f32,
    pub min_firing_value: f32,
}
impl Default for NetConfig {
    fn default() -> Self {
        NetConfig {
            min_weight_magnitude: 0.,
            min_firing_value: 0.01, //f32::MIN, // Minimum firing value for a neuron to "fire" to its "children". Set to f32::MIN to ALWAYS fire.. Note that it takes absolute value before making a check, so negative values will fire just fine
        }
    }
}
#[derive(Debug, Clone)]
pub struct Net {
    pub neurons: Vec<Neuron>,
    input_neurons: usize,
    output_neurons: usize,
    config: NetConfig,
    finalized: bool,
    output_activation: NeuronType,
}
impl Net {
    // Creates a new network and initializes the inputs
    pub fn new(input_size: usize, output_size: usize, output_activation: NeuronType) -> Net {
        let mut v: Vec<Neuron> = Vec::with_capacity(10000);

        for _ in 0..input_size {
            v.push(Neuron::default());
        }
        Net {
            neurons: v,
            input_neurons: input_size,
            output_neurons: output_size,
            config: Default::default(),
            finalized: false,
            output_activation,
        }
    }

    // Adds a single neuron, not connected to anything.
    pub fn add_neuron(&mut self, neuron: Neuron) {
        debug_assert!(!self.finalized, "Network was finalized!");
        self.neurons.push(neuron);
    }

    // Adds output neurons (call it after adding all hidden neurons)
    pub fn finalize_network(&mut self) {
        debug_assert!(
            !self.finalized,
            "Attempting to finalize the same network twice!"
        );
        self.finalized = true;
        for _ in 0..self.output_neurons {
            self.neurons.push(Neuron::typed(self.output_activation));
        }
    }

    pub fn get_non_output_neuron_count(&self) -> usize {
        self.get_hidden_neuron_count() + self.input_neurons
    }

    // Returns the number of hidden neurons in the network
    pub fn get_hidden_neuron_count(&self) -> usize {
        self.neurons.len() - self.output_neurons - self.input_neurons
    }

    pub fn get_outputs_count(&self) -> usize {
        self.output_neurons
    }

    pub fn get_inputs_count(&self) -> usize {
        self.input_neurons
    }

    pub fn get_neuron_link_count(&mut self, neuron: usize) -> usize {
        debug_assert!(
            self.neurons.len() > neuron,
            "Getting link count of a non existing neuron!"
        );
        self.neurons[neuron].get_links().len()
    }

    fn neuron_in_bounds(&self, id: usize) -> bool {
        self.neurons.len() > id
    }

    // Writes a networks weights to another network. Use it to limit deepcopies
    pub fn write_to(&self, other: &mut Net) {
        for n in self.neurons.iter().zip(other.neurons.iter_mut()) {
            for link in n.0.links.iter().zip(n.1.get_links()) {
                *link.1 = *link.0;
            }
        }
    }

    // Fills a neural network with random weights
    pub fn reseed(&mut self) {
        use nanorand::{Rng, WyRand};
        let mut rng = WyRand::new();
        for n in &mut self.neurons {
            for link in n.get_links() {
                // let w = rng.generate::<f32>();
                let w = (rng.generate::<f32>() - 0.5) * 2.;
                *link = (link.0, w);
            }
        }
    }

    pub fn set_link(&mut self, from: usize, to: usize, weight: f32) {
        debug_assert!(
            self.neuron_in_bounds(from) && self.neuron_in_bounds(to),
            "Neuron out of bounds!"
        );
        debug_assert!(from != to, "You can't create link loops!");
        let borrowed = self.neurons[from].get_links();
        if weight.abs() < self.config.min_weight_magnitude {
            // Since the weight is 0, we can just remove the link if it's in the neuron
            // If it isn't, there's nothing to do anyway.
            let mut index = 0;
            let mut found = false;
            for b in borrowed.iter().enumerate() {
                if b.1 .0 == to {
                    found = true;
                    index = b.0;
                    break;
                }
            }
            if found {
                borrowed.remove(index);
            }
        } else {
            let mut index = 0;
            let mut found = false;
            for b in borrowed.iter().enumerate() {
                if b.1 .0 == to {
                    found = true;
                    index = b.0;
                    break;
                }
            }
            if found {
                borrowed[index] = (to, weight);
            } else {
                borrowed.push((to, weight));
            }
        }
    }

    // Modifies a link given the downstream link neuron and an index into its link array
    pub fn modify_link(&mut self, from: usize, nth_link: usize, weight_delta: f32) {
        debug_assert!(self.neuron_in_bounds(from), "Neuron out of bounds!");
        let borrowed = self.neurons[from].get_links();
        debug_assert!(
            borrowed.len() != 0,
            "Modifying links of a neuron with no links!"
        );
        let nth = nth_link % borrowed.len();
        let new_weight = borrowed[nth].1 + weight_delta;
        if new_weight.abs() < self.config.min_weight_magnitude {
            // Weight magnitude is very small, we can remove the neuron (saves some processing time...)
            // TODO: ^
        } else {
            // println!(
            // "Modifying: {} to {} from {}",
            // nth, new_weight, borrowed[nth].1
            // );
            // NOTE: weight clipping
            borrowed[nth].1 = new_weight.max(-5.).min(5.);
        }
    }

    // Wipes neuron values. Use it before evaluating a batch.
    // Note that it wipes ALL values so it's not usable for evaluating on RNNs.
    fn clear_old_values(&mut self) {
        // Loop over all neurons and wipe their values
        for n in &mut self.neurons {
            n.clear();
        }
    }

    pub fn prepare_for_next_sequence(&mut self) {
        self.clear_old_values();
    }

    // Evaluates the network and given inputs and writes outputs
    // Returns the number of spikes fired by the network during evaluation
    pub fn evaluate(
        &mut self,
        inputs: &[f32],
        outputs: &mut [f32],
        hidden_state_size: usize,
        prepare_hidden_state: &dyn Fn(&Net, &mut Vec<f32>) -> (),
        apply_hidden_state: &dyn Fn(&mut Net, &Vec<f32>) -> (),
    ) -> usize {
        let mut spikes = 0;

        debug_assert!(self.finalized, "Network wasn't finalized!");
        // Ensure correct arguments
        // Given inputs evaluates the network
        debug_assert!(
            inputs.len() == self.input_neurons,
            "Inputs have the wrong size!\nDesired: {}\nActual: {}",
            self.input_neurons,
            inputs.len(),
        );
        debug_assert!(
            outputs.len() == self.output_neurons,
            "Outpus have the wrong size!\nDesired: {}\nActual: {}",
            self.output_neurons,
            outputs.len(),
        );

        // Prepare hidden state
        let mut hidden = vec![0.; hidden_state_size];
        prepare_hidden_state(self, &mut hidden);
        // Clear old data
        self.clear_old_values();
        // Apply hidden state
        apply_hidden_state(self, &hidden);

        let mut_ptr = self.neurons.as_mut_ptr();
        // Write input neurons
        for i in 0..self.input_neurons {
            let n = &mut self.neurons[i];
            n.add(inputs[i]);
        }
        // Evaluate (this is technically fully connected feed forward as of now...)
        for n in &mut self.neurons {
            let evaled_inputs = n.evaluate();
            if evaled_inputs.abs() > self.config.min_firing_value {
                let links = n.get_links();
                for l in links {
                    let linked = unsafe { &mut *mut_ptr.offset(l.0 as isize) };
                    linked.add(evaled_inputs * l.1);
                    spikes += 1;
                }
            }
        }

        // Write outputs
        let hidden = self.get_hidden_neuron_count();
        for o in 0..self.output_neurons {
            outputs[o] = self.neurons[self.input_neurons + hidden + o].evaluate();
        }
        // Softmaxify before returning
        let mut output_sum = 0.;
        const SOFTMAX_MIN: f32 = 0.00001;
        for o in outputs.iter() {
            output_sum += (*o).max(SOFTMAX_MIN).exp();
        }
        for o in outputs.iter_mut() {
            *o = (*o).max(SOFTMAX_MIN).exp() / output_sum;
        }

        spikes
    }

    pub fn print_params(&self) {
        let mut edge_count = 0;
        for n in &self.neurons {
            edge_count += n.links.len();
        }

        println!("Network parameters: {}", edge_count);
        println!("Network neurons: {}", self.neurons.len());
        println!("Network inputs: {}", self.input_neurons);
        println!("Network outputs: {}", self.output_neurons);
    }
}
