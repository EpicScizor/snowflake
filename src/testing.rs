pub fn test_squaring() -> (Vec<Vec<f32>>, Vec<Vec<f32>>) {
    let mut inputs = vec![];
    let mut outputs = vec![];
    for x in 0..100 {
        let i = x as f32 / 100.;
        inputs.push(vec![i]);
        outputs.push(vec![i * i]);
    }
    (inputs, outputs)
}

pub fn test_two_dimensions() -> (Vec<Vec<f32>>, Vec<Vec<f32>>) {
    let mut inputs = vec![];
    let mut outputs = vec![];
    for x in 0..10 {
        for y in 0..10 {
            let i = x as f32 / 3.1415 / 3.;
            let j = y as f32 / 2.435789 / 3.;
            inputs.push(vec![i, j]);
            outputs.push(vec![i * i - j + j * i + (j.cos() + i).sin()]);
        }
    }
    (inputs, outputs)
}
