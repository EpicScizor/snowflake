use std::path::PathBuf;

// Returns a byte vector from file (best used on text files...)
pub fn file_to_text_training_vector(file_path: PathBuf) -> Vec<u8> {
    let mut bytes = std::fs::read_to_string(file_path).unwrap().into_bytes();
    for b in &mut bytes {
        *b = ((*b).max(32).min(126) as isize - 32) as u8; // 95 valid characters
    }
    bytes
}
