pub mod annealing;
pub mod data;
pub mod spiking;
pub mod testing;

use anyhow::Result;
use std::cell::RefCell;
use std::rc::Rc;

use crate::annealing::Annealing;
use crate::spiking::{Net, Neuron};

// Prints a borrowed vector with up to 4 decimal places of precision
fn pretty_print_vector(v: &Vec<f32>) {
    println!("{{");
    for f in v {
        print!("{:.4}, ", f);
    }
    println!("\n}}");
}

pub fn main() -> Result<()> {
    // Load text...
    println!("===");
    let trained =
        data::file_to_text_training_vector("/home/desu/rust/nn/data/shakespeare.txt".into());
    println!("===");

    let inputs: usize = 95 as usize;
    const HIDDEN: usize = 128;
    let outputs: usize = 95 as usize;

    const SEQUENCE_SIZE: usize = 64;

    let mut nn = spiking::Net::new(inputs, outputs, spiking::NeuronType::Sigmoid);
    // Make a "layer" holding hidden state data for recursion
    for _ in 0..HIDDEN {
        nn.add_neuron(Neuron::default());
    }
    // Make a "layer" for recursive states
    for _ in 0..HIDDEN {
        nn.add_neuron(Neuron::tanh());
    }
    // make a second "layer"
    for _ in 0..HIDDEN {
        nn.add_neuron(Neuron::sigmoid());
    }
    nn.finalize_network();

    // Connect inputs to the "layer"
    for input in 0..inputs {
        for hidden in 0..HIDDEN {
            nn.set_link(input, inputs + HIDDEN + hidden, 0.);
        }
    }
    // Recursive state layer
    for hidden in 0..HIDDEN {
        for output in 0..HIDDEN {
            nn.set_link(inputs + hidden, inputs + HIDDEN + output, 0.);
        }
    }
    for hidden in 0..HIDDEN {
        for output in 0..HIDDEN {
            nn.set_link(
                inputs + HIDDEN + hidden,
                inputs + HIDDEN + HIDDEN + output,
                0.,
            );
        }
    }
    // Connect the hidden layer to the output layer
    for hidden in 0..HIDDEN {
        for output in 0..outputs {
            nn.set_link(
                inputs + HIDDEN + HIDDEN + hidden,
                inputs + HIDDEN + HIDDEN + HIDDEN + output,
                0.,
            );
        }
    }
    nn.reseed();

    // Besides preparing the network architecture, we also need to create the necessary recursive hidden state bookkeeping
    let prepare_hidden_state = |net: &Net, hidden: &mut Vec<f32>| {
        for h in 0..HIDDEN {
            hidden[h] = net.neurons[inputs + HIDDEN + h].value;
        }
    };
    let apply_hidden_state = |net: &mut Net, hidden: &Vec<f32>| {
        for h in 0..HIDDEN {
            let neur = &mut net.neurons[inputs + h];
            neur.value = hidden[h];
        }
    };

    // Log out some debugging info
    nn.print_params();

    // Train
    let mut best = nn.clone();
    let mut best_score = f32::MAX;
    // Try up to 5 times...
    for _attempts in 0..10 {
        // let mut annealer: Annealing = annealing::annealing(0.9999999, 1.);
        let mut annealer: Annealing = annealing::annealing(0.99999, 1.);
        let mut nn = best.clone();
        nn.reseed();

        println!("===");

        let start = std::time::Instant::now();
        let mut mirrored = Rc::new(RefCell::new(nn.clone()));
        let mut held = Rc::new(RefCell::new(nn));
        const ANNEALING_STEPS: usize = 30000000;
        for i in 0..ANNEALING_STEPS {
            // let start = Instant::now();
            let (loss, spikes) = annealer.anneal_on_text(
                &mut held,
                &mut mirrored,
                &trained,
                i % (trained.len() - 2 * SEQUENCE_SIZE),
                SEQUENCE_SIZE,
                HIDDEN,
                &prepare_hidden_state,
                &apply_hidden_state,
            );
            // println!("Text: {:?}", &trained[0..SEQUENCE_SIZE]);
            // println!("{}", (Instant::now() - start).as_secs_f64());
            // if i % 100000 == 10000 {
            if i % 10000 == 10 {
                let mut pred = Annealing::predict_text(
                    &mut held.borrow_mut(),
                    &vec![],
                    SEQUENCE_SIZE,
                    HIDDEN,
                    &prepare_hidden_state,
                    &apply_hidden_state,
                );
                for p in &mut pred {
                    *p += 32;
                }
                println!(
                    "{:?}\n{:?}",
                    pred,
                    std::str::from_utf8(&pred).unwrap_or("<parsing error>")
                );
                println!(
                    "Loss: {},     Spikes: {},     Progress: {}%     Temperature: {},     Annealer current failures: {}     ETA: {}d",
                    loss,
                    spikes,
                    i as f32 / ANNEALING_STEPS as f32 * 100.,
					annealer.temperature,
					annealer.failures,
					ANNEALING_STEPS as f32 / i as f32 * (std::time::Instant::now() - start).as_secs_f32() / 60. / 60. / 24.
                );
            }
            // if annealer.failures > 100000 {
            // println!("Breaking at {}%", 100. * i as f32 / ANNEALING_STEPS as f32);
            // break;
            // }
        }
        let end = std::time::Instant::now();
        let mut nn = (*(*held).borrow_mut()).clone();
        let (loss, spikes) = Annealing::eval_on_text(
            &mut nn,
            &trained,
            0,
            SEQUENCE_SIZE,
            HIDDEN,
            &prepare_hidden_state,
            &apply_hidden_state,
        );
        println!("Loss: {},    Spikes: {}", loss, spikes);
        println!("Annealing time: {}", (end - start).as_secs_f64());
        if loss < best_score {
            best_score = loss;
        }
        best = nn;
    }

    return Ok(());
}
