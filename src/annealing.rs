use std::{cell::RefCell, rc::Rc};

use crate::{pretty_print_vector, spiking::Net};

#[derive(Default, Debug, Copy, Clone)]
pub struct Annealing {
    pub temperature: f32,
    pub cooling_rate: f32,
    pub failures: usize, // consecutive failures to improve the score
    pub diagnostic_total_failures: usize,
    pub diagnostic_total_anneals: usize,
    pub step_counter: usize,
    pub learning_rate: f32,
}

pub fn annealing(cooling_rate: f32, learning_rate: f32) -> Annealing {
    Annealing {
        temperature: 1.,
        cooling_rate,
        failures: 0,
        diagnostic_total_anneals: 0,
        diagnostic_total_failures: 0,
        step_counter: 0,
        learning_rate,
    }
}

impl Annealing {
    pub fn anneal_on_text(
        &mut self,
        original_network: &mut Rc<RefCell<Net>>,
        cloned_network: &mut Rc<RefCell<Net>>,
        text: &Vec<u8>,
        start: usize,
        size: usize,
        hidden_state_size: usize,
        prepare_hidden_state: &dyn Fn(&Net, &mut Vec<f32>) -> (),
        apply_hidden_state: &dyn Fn(&mut Net, &Vec<f32>) -> (),
    ) -> (f32, usize) {
        assert!(text.len() > 0, "Empty string!");

        // Calculate loss on the based network for comparison
        let (initial_loss, initial_spikes) = Self::eval_on_text(
            &mut (**original_network).borrow_mut(),
            text,
            start,
            size,
            hidden_state_size,
            prepare_hidden_state,
            apply_hidden_state,
        );
        let initial_spikes = initial_spikes / size;

        // Make a copy of the original network and perturb it
        (**original_network)
            .borrow_mut()
            .write_to(&mut (**cloned_network).borrow_mut());
        self.perturb(&mut (**cloned_network).borrow_mut());

        // Evaluate loss on the cloned network
        let (cloned_loss, cloned_spikes) = Self::eval_on_text(
            &mut (**cloned_network).borrow_mut(),
            text,
            start,
            size,
            hidden_state_size,
            prepare_hidden_state,
            apply_hidden_state,
        );
        let cloned_spikes = cloned_spikes / size;

        let original_cloned_loss = cloned_loss;
        // TODO: Decide if this is worth it (scaling by spike count)
        // let cloned_loss = cloned_loss * (cloned_spikes as f32 / initial_spikes as f32 / 2. + 0.5);

        // Annealing requires us to perturb the model
        // println!("{} vs {}", cloned_loss, initial_loss);
        let p_accept = (-(cloned_loss - initial_loss) / (50. * self.temperature)).exp();
        use nanorand::{Rng, WyRand};
        let mut rng = WyRand::new();

        if cloned_loss <= initial_loss || rng.generate::<f32>() < p_accept {
            // Swap 'em around!
            let old = original_network.clone();
            *original_network = cloned_network.clone();
            *cloned_network = old;
            self.failures = 0;
        } else {
            self.failures += 1;
            self.diagnostic_total_failures += 1;
        }
        self.diagnostic_total_anneals += 1;

        self.step_counter += 1;
        self.temperature *= self.cooling_rate;
        // println!("Temperature: {}", self.temperature);
        if cloned_loss < initial_loss {
            (original_cloned_loss, cloned_spikes)
        } else {
            (initial_loss, initial_spikes)
        }
    }

    // Perturbs a neural network a bit
    fn perturb(&self, net: &mut Net) {
        let nc = net.get_non_output_neuron_count();

        use nanorand::{Rng, WyRand};
        let mut rng = WyRand::new();

        for n in 0..nc {
            for l in 0..net.get_neuron_link_count(n) {
                let weight_delta =
                    (rng.generate::<f32>() - 0.5) * 2. * self.temperature * self.learning_rate;
                net.modify_link(n, l, weight_delta);
            }
        }
    }

    pub fn eval_on_text(
        net: &mut Net,
        input_vector: &Vec<u8>,
        start: usize,
        size: usize,
        hidden_state_size: usize,
        prepare_hidden_state: &dyn Fn(&Net, &mut Vec<f32>) -> (),
        apply_hidden_state: &dyn Fn(&mut Net, &Vec<f32>) -> (),
    ) -> (f32, usize) {
        //
        net.prepare_for_next_sequence();

        //
        let mut spikes = 0;
        let mut loss = 0.;

        // Construct new inputs
        let mut inputs = vec![0.; 95];
        let mut outputs = vec![0.; 95];
        // Clear inputs
        let clear_vec = |v: &mut Vec<f32>| {
            for i in 0..v.len() {
                v[i] = 0.;
            }
        };

        for offset in 0..size {
            // Clear old vectors
            clear_vec(&mut inputs);
            clear_vec(&mut outputs);

            // Run predictions
            let current = input_vector[start + offset];
            let prediction = input_vector[start + offset + 1];
            inputs[current as usize] = 1.;
            spikes += net.evaluate(
                &inputs,
                &mut outputs,
                hidden_state_size,
                prepare_hidden_state,
                apply_hidden_state,
            );

            // Only try to predict the last 10 tokens (maybe itll make training better?)
            // Calculate loss...
            for (index, o) in outputs.iter().enumerate() {
                let target = if index as u8 == prediction { 1. } else { 0. };
                let x = *o - target;
                loss += x * x; // square it because it makes it more costly to be off by a lot
            }
            /*
            let mut best_index = 0;
            let mut best_value = f32::MIN;
            for (index, value) in outputs.iter().enumerate() {
                if *value > best_value {
                    best_value = *value;
                    best_index = index;
                }
            }
            if best_index as u8 != prediction {
                loss += 10.;
            }
            */
        }

        //
        (loss / size as f32 / outputs.len() as f32, spikes)
    }

    pub fn predict_text(
        net: &mut Net,
        prompt: &Vec<u8>,
        to_predict: usize,
        hidden_state_size: usize,
        prepare_hidden_state: &dyn Fn(&Net, &mut Vec<f32>) -> (),
        apply_hidden_state: &dyn Fn(&mut Net, &Vec<f32>) -> (),
    ) -> Vec<u8> {
        //
        net.prepare_for_next_sequence();
        //
        // Construct new inputs
        let mut inputs = vec![0.; 95];
        let mut outputs = vec![0.; 95];
        // Clear inputs
        let clear_vec = |v: &mut Vec<f32>| {
            for i in 0..v.len() {
                v[i] = 0.;
            }
        };

        let mut output_vector = vec![];

        for v in prompt {
            // Clear old vectors
            clear_vec(&mut inputs);
            clear_vec(&mut outputs);

            // Run predictions, just to advance the hidden state.
            inputs[*v as usize] = 1.;
            net.evaluate(
                &inputs,
                &mut outputs,
                hidden_state_size,
                prepare_hidden_state,
                apply_hidden_state,
            );
        }
        // Actual prediction
        for _ in 0..to_predict {
            // Clear old vectors
            clear_vec(&mut inputs);
            clear_vec(&mut outputs);

            // Run predictions, just to advance the hidden state.
            if output_vector.len() > 0 {
                inputs[*output_vector.last().unwrap() as usize] = 1.;
            } else {
                use nanorand::{Rng, WyRand};
                let mut rng = WyRand::new();
                inputs[rng.generate_range(32..127) - 32] = 1.;
            }
            net.evaluate(
                &inputs,
                &mut outputs,
                hidden_state_size,
                prepare_hidden_state,
                apply_hidden_state,
            );

            // pretty_print_vector(&outputs);

            // Sample probabilistically.
            // We're squaring to decrease the odds of trash outputs.
            let mut total = 0.0;
            for value in &outputs {
                total += *value * *value;
            }
            use nanorand::{Rng, WyRand};
            let mut rng = WyRand::new();
            let threshold: f32 = rng.generate::<f32>() * total;
            let mut counter = 0.;
            let mut final_index = 0;
            for (index, value) in outputs.iter().enumerate() {
                counter += *value * *value;
                if counter >= threshold {
                    final_index = index;
                    break;
                }
            }

            output_vector.push(final_index as u8);
        }

        return output_vector;
    }
}
